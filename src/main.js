

jQuery(function($) {

  $(document).ready(function () {

    $('a[data-rel^=lightcase]').lightcase();

    $('.header-bot__burger .inner').on('click', function () {

      $('.header').toggleClass('burger-opened');

    });

    var eventFired = 0;

    if ($(window).width() > 1200 - 18) {
      $('.goods__item').removeClass('opened');
      $('.goods-content .goods__item:first-child').addClass('opened');
      console.log('desktop')
    }
    else {
      console.log('mobile')
    }

    $(window).on('resize', function() {
      if (!eventFired) {
        if ($(window).width() > 1200 - 18) {
          $('.goods__item').removeClass('opened');
          $('.goods-content .goods__item:first-child').addClass('opened');
          console.log('desktop')
        }
        else {
          console.log('mobile')
        }
      }
    });

    $('.goods__item-title').on('click', function () {

      var waf = $(this).closest('.goods__item');

      if ($(waf).hasClass('opened')){

        if ($(window).width() < 1200 - 18){
          $(waf).removeClass('opened');
        }

      }  else {
        $('.goods__item.opened:not(waf)').removeClass('opened');
        $(waf).addClass('opened');
      }
    });



    $('.gallery-wrap__button .def-button').on('click', function () {
      $('.gallery-wrap-hide').slideToggle();
    });


    $(".owl-carousel").owlCarousel({
      loop: true,
      margin: 30,

      responsive: {
        0: {
          dots: true,
          nav: false,
          items: 1
        },

        768: {
          dots: false,
          items: 1,
          nav: true,
          navText: ''
        },

        1200: {
          dots: false,
          nav: true,
          navText: '',
          items: 1
        }
      }
    });

    var topItem = 0,
        leftItem = 0,
        popupHeight = 500;

    $(".owl-carousel .item").on("click", function(e) {
      var $this = $(this),
          $parent = $this.parents(".review-wrap"),
          content = $this.html(),
          $popup = $parent.find(".popup");

      topItem = $this.offset().top - $parent.offset().top + $this.height() / 2;
      leftItem = $this.offset().left - $parent.offset().left + $this.width() / 2;

      $popup.css({
        top: topItem,
        left: leftItem,
        width: 0,
        height: 0
      });

      $popup.html(content).stop().animate(
          {
            top: -((popupHeight - $this.parent().outerHeight()) / 2),
            left: 0,
            width: "100%",
            height: popupHeight,
            opacity: 1
          },
          400
      );
    });

    $(".popup").on("click", function(e) {
      $(this).stop().animate(
          {
            width: 0,
            height: 0,
            top: topItem,
            left: leftItem,
            opacity: 0
          },
          400
      );
    });





    //  PRODUCTS POPUP START

    $('.products__item > span').on('click', function () {

      $(this).closest('.products__item').find('.products__item-popup').fadeIn(300);

    });

    $('.products__item-popup__background').on('click', function () {

      $(this).closest('.products__item-popup').fadeOut(300);

    });

    $('.products__item-popup__inner-button').on('click', function () {

      var titlePopup = $(this).closest('.products__item-popup').find('.products__item-popup__inner-title').html();

      $('#form-popup input[type="hidden"]').val(titlePopup);
      $('#form-popup p').html(titlePopup);

      $(this).closest('.products__item-popup').fadeOut(300);

      $('#form-popup').fadeIn(600);
    });

    $('.form-popup__back').on('click', function () {

      $('#form-popup').fadeOut(300);

    });

    //  PRODUCTS POPUP END




  });

});


